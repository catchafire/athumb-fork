from django.template import Library

from athumb.templatetags.thumbnail import thumbnail
register = Library()

register.tag(thumbnail)
